import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class StringCalculator {
	public static int add(String numbersWithDelimiters) {
		int sum = 0;
		String numbers = numbersWithDelimiters;
	    final String endOfCustomerDelimiters = "\n";
		if(!numbersWithDelimiters.equals("")) {
			String delimitersStart = "//";
			String delimiters = "";
			if (numbersWithDelimiters.startsWith(delimitersStart)) {
				delimiters = getDelimiters(numbersWithDelimiters.substring(delimitersStart.length(), numbersWithDelimiters.indexOf(endOfCustomerDelimiters)));
				numbers = numbersWithDelimiters.substring(numbersWithDelimiters.indexOf(endOfCustomerDelimiters) + endOfCustomerDelimiters.length());
			} else {
				delimiters = getDelimiters("");
			}
			sum = getSum(numbers, delimiters);
		}
        return sum;
    }
	
	public static String getDelimiters(String delimitersString) {
		String defaultDelimiters = ",|\n";
		String delimiters = "";
		String[] delimitersArray = delimitersString.split("]");
		for (String customDelimiter : delimitersArray) {
			if(customDelimiter.startsWith("[")) {
				if(!delimiters.isEmpty()) {
					delimiters += "|";
				}
				delimiters += Pattern.quote(customDelimiter.substring(1, customDelimiter.length()));
			}
		}
		if(delimiters.isEmpty()) {
			delimiters = defaultDelimiters;
		} 
		return delimiters;
	}
	
	public static int getSum(String numbers, String delimiters) {
		String[] numbersArray = numbers.split(delimiters);
	    List<String> negativeNumbers = new ArrayList<String>();
	    int sum = 0;
		for (String number : numbersArray) {
			int numberValue = Integer.parseInt(number.trim());
            if (numberValue < 0) {
                negativeNumbers.add("" + numberValue);
			} else if (numberValue > 1000) {
				continue;
			}
			sum += Integer.parseInt(number); 
		}
		if(!negativeNumbers.isEmpty()) {
			throw new RuntimeException("There is negatives in numbers: " + negativeNumbers.toString());
		}
		return sum;
	}
}
