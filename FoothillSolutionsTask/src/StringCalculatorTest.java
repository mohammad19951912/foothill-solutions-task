import org.junit.Assert;
import org.junit.Test;

public class StringCalculatorTest {

	@Test
	public void whenAddWithEmptyString() {
		 Assert.assertEquals(0, StringCalculator.add(""));
	}
	@Test
	public final void whenAddOneNumber() {
		Assert.assertEquals(1,StringCalculator.add("1"));
	}
	@Test
	public final void whenAddTwoNumbers() {
		Assert.assertEquals(3,StringCalculator.add("1,2"));
	}
	@Test(expected = RuntimeException.class)
	public final void whenStringHaveNotNumber() {
		StringCalculator.add("1,x");
	}
	@Test
	public final void whenHanleUnknownAmountOfNumbers() {
		Assert.assertEquals(1+2+3+4+5+6+7+8+9, StringCalculator.add("1,2,3,4,5,6,7,8,9"));
	}
	@Test
	public final void whenSpiltUsingNewLine() {
		Assert.assertEquals(1+2+3+4+5, StringCalculator.add("1,2\n3,4\n5"));
	}
	@Test(expected = RuntimeException.class)
	public final void whenTwoSplitersAfterEachOther() {
		StringCalculator.add("1,2\n\n4");
	}
	@Test
	public final void whenDelimiterIsSpecifiedThenItIsUsedToSeparateNumbers() {
	    Assert.assertEquals(1+12+123, StringCalculator.add("//[&]\n1&12&123"));
	}
	
	@Test
	public final void whenNegativeNumberInNumbers() {
		Exception exception = null;
	    try {
	        StringCalculator.add("1,-1");
	    } catch (Exception runtimeException) {
	        exception = runtimeException;
	    }
	    Assert.assertNotNull(exception);
	    Assert.assertEquals("There is negatives in numbers: [-1]", exception.getMessage());
	}
	
	@Test
	public final void whenOneOrMoreNumbersAreGreaterThan1000() {
	    Assert.assertEquals(1+12+123+1000, StringCalculator.add("1,12,123,1000,1001"));
	}
	
	@Test
	public final void whenDelimiterWithAnyLengthIsSpecifiedThenItIsUsedToSeparateNumbers() {
	    Assert.assertEquals(1+2+3, StringCalculator.add("//[**]\n1**2**3"));
	}
	
	@Test
	public final void whenMoreThanOneCustomDelimiterIsSpecifiedThenItIsUsedToSeparateNumbers() {
	    Assert.assertEquals(1+2+3, StringCalculator.add("//[*][%]\n1*2%3"));
	}
	
	@Test
	public final void whenMoreThanOneCustomDelimiterWithAnyLengthIsSpecifiedThenItIsUsedToSeparateNumbers() {
	    Assert.assertEquals(1+2+3, StringCalculator.add("//[**][%%]\n1**2%%3"));
	}
	
	

	   
}
